#Me permettait de mettre les donnees deja traitees dans le csv, je ne l'execute que si je modifie mes donnees ou si mon csv est vide

import spacy # importation de la librairie
import csv # importation de la librairie permettant de creer le fichier csv
import time


start = time.time()
# Fonction de tokenisation
def return_token(sentence):

  nlp = spacy.load("fr_core_news_sm")
  # Tokeniser la phrase
  doc = nlp(sentence)
  # Retourner le texte de chaque token
  return [X.text for X in doc]


# Extraction du corpus avec ponctuation
liste = []
with open("corpus_afb.txt", "r", encoding='latin-1') as f:
  for line in f.readlines():
    liste.append(line)
 

punc = [':', "!", "^", '.', '`', '~', ';', ',', '?', '...', '_', ' ']
list_index_valid = [] # pour stocker les index uniques des phrases à conserver

count = 0 # pour compter le nombre de lignes du fichier csv
with open('references_afb.csv', 'a', encoding='UTF8') as f : # j'ouvre le fichier en ecriture pour stocker les donees pretraitees
    writer = csv.writer(f) # variable du csv permettant d'ecrire dans le csv
    # Tokenisation du corpus, tout en evitant les redondances
    for line in liste: # je parcours chaque phrase du fichier des conversations
      print(liste.index(line), count) # j'affiche l'index de la phrase et la taille actuelle du csv
      if liste.index(line) in list_index_valid: # si cest une phrase qui est deja dans la liste des phrases deja traitees, je passe direct à la phrase suivante
        print("element deja present avec l'indice", liste.index(line)) # j'affiche son index 
        continue
      else:
        list_index_valid.append(liste.index(line)) # si la phrase n'a pas encore ete traitee, je garde son index pour plutard rechercher  si son inndex est present et savoir si je doi traiter ou non
      line = line.lower() # pour convertir toues les phrases en minuscules
      result = return_token(line)[:-1] # je tokenise donc la phrase
      liste_tokens = []
      for token in result: # pour chaque token, s'il n'est pas une ponctuation je le garde
        if token not in punc :
          liste_tokens.append(token) # je le garde dans la liste que j'ai cree plutard
      suite_token = "" # j'initialise la variable qui va me permettre de concatener et mettre dans le fichier
      for token in liste_tokens: # je concatene chque token qui n'etait pas la ponctuation
        suite_token = suite_token + token + ' '
      writer.writerow(suite_token[:-1]) # je save dans le csv
      count = count + 1 # j'increment pour avoir l'indice de la derniere ligne du csv

end = time.time()
print("le temps mis est de :", end - start, "secondes")