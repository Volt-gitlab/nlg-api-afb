# -*- coding: utf-8 -*-
""" Construction d'une api offrant les services d'un modele NLG entrainé avec Ngram"""
# 1. Preparation des donnnes d'entrainement



from flask import Flask, request
from flask_restful import Resource, Api
from NgramModel import NgramModel
import csv # importation de la librairie permettant de creer le fichier csv
import time
import pickle

import json
import random
import numpy as np

app = Flask(__name__, template_folder='../templates', static_folder='../static') # creation de l'aplication Flask
api = Api(app) # Creation de l'api

corpus = []
with open('references_afb.csv', 'r', encoding = 'UTF8') as file:
    
    reader = csv.reader(file)
    
    # i get data
    for i in reader:
        ref = ''.join(i)
        corpus.append(ref.split(' '))


#Importons le modele entrainé
#OPen the file in binar mode
with open('models.pkl', 'rb') as file:

    #call load methode to deserialize variable
    var_modele = pickle.load(file)



def modele(sentence):

  modele = var_modele # copie du modele deja entrainé
  result = modele.generate_sentence(sentence) # generation de texte
  if type(result) == str: # au cas ou il devait me retourner un message d'erreur
    return []
  return result


# 3. Creation de l'endpoint qui genere la phrase finale

all_candidats = []

class GetSentence(Resource):

  def post(self):
    start = time.time()
    score_max = 0
    final_sentence = " I don't understand what you mean ..."
    list_final_sentence = []
    sentence = request.form['data']
    sentence = sentence.lower()
    if sentence == "" or sentence ==" ":
      return json.dumps({"message_error" : " Enter sentence with at least one character"}), 400
    else :  
      all_candidats = modele(sentence) + modele(sentence) + modele(sentence) + modele(sentence)
      for candidat in all_candidats:
        candidat_list = var_modele.return_token(candidat) # je transforme en liste de tokens pour passer a la fonction d'evaluation
        score = var_modele.evaluate(references = corpus, candidat = candidat_list, weights=(0.05, 0.15, 0.3, 0.5)) # les poids sont organisés de la sorte pour privilegier les modeles dont les generations sont egales en taille de 4
        if score >= score_max and score != 1:
          score_max = score
          final_sentence = candidat
        elif score == 1:
          list_final_sentence.append(candidat) # Pour garder ceux dont le score est 1, et faire un random parmi

    end = time.time()

    print(end - start, "secondes")

    if len(all_candidats) <= 1 :
      return json.dumps({"sentence" : final_sentence}), 400 

    elif len(list_final_sentence) == 0 :
      final_sentence = final_sentence.capitalize()
      return json.dumps({"sentence" : final_sentence})

    else:
      print("plusieurs")
      return json.dumps({"sentence" : random.choice(list_final_sentence).capitalize()})


api.add_resource(GetSentence, '/generate/')

if __name__ == '__main__':
    app.run(debug=True)
