from NgramModel import NgramModel
import csv
import pickle # for save variables(state of project)
import time

start = time.time()

corpus = []
# recuperation des references dans le fichier csv
with open('references_afb.csv', 'r', encoding='UTF8') as file :
    
    reader = csv.reader(file)

    # i get data
    for i in reader:
      ref = ''.join(i)
      corpus.append(ref.split(' '))

# 2 Entrainement des differents modeles
"""
first_modele_5 = NgramModel(corpus, 5)
print("1")
second_modele_7 = NgramModel(corpus, 7)
print("2")
third_modele_9 = NgramModel(corpus, 9)
print("3")
"""
fourth_modele_11 = NgramModel(corpus, 11)
#print("4")

# Open a file and use dump()
with open('models.pkl', 'wb') as file:
      
    # A new file will be created for keep trained model
    pickle.dump(fourth_modele_11, file)

end = time.time()

print("Training has made ",end - start, "secondes")
